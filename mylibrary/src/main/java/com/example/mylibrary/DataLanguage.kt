package com.example.mylibrary

import android.content.Context

class DataLanguage {

    fun getTitle(context: Context) : String = context.getString(R.string.title)
}