package com.example.yothinindy.myapplication

import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import com.example.mylibrary.DataLanguage
import kotlinx.android.synthetic.main.activity_main2.*

class Main2Activity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)


        val changeLanguageBtn = findViewById<Button>(R.id.changeLanguageBtn)
        changeLanguageBtn.text = getString(R.string.changeLa)
        changeLanguageBtn.setOnClickListener {
            if (LocaleManager.getInstance(this).currentLocal.language == "en")
                LocaleManager.getInstance(this).setLocale("th")
            else
                LocaleManager.getInstance(this).setLocale("en")

            recreateActivity(false)
        }

        nameTv.text = DataLanguage().getTitle(this)
    }

    fun recreateActivity(animation: Boolean) {
        val intent = intent
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
        finish()
        if (animation) {
            overridePendingTransition(android.R.anim.fade_out, android.R.anim.fade_in)
        } else {
            overridePendingTransition(0, 0)
        }

        startActivity(intent)
        if (animation) {
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
        } else {
            overridePendingTransition(0, 0)
        }
    }

    override fun attachBaseContext(newBase: Context?) {
        super.attachBaseContext(LocaleManager.getInstance(newBase!!).wrapContext())
    }
}
